function mostracaricamento(name)
{
  Swal.fire({
  position: 'center',
  //type: 'warning',
  title: 'Attendere',
  html: name,
  imageUrl: 'dist/src/load.png',
  showConfirmButton: false,
  allowOutsideClick: false,
 
  //width:"10%"
  //timer: 2500
})
//Swal.showLoading()

window.onbeforeunload = function() {
    return 'Vuoi uscire?';
};

}

function makerequest(url, time=-1)
{  
    var event=null;
    var backtext="";
    var icon="";
    var pre="";
    if((url.indexOf("led&")>0))
    {
      event="led";
      backtext="p-3 mb-2 bg-light text-dark";
      pre="Accensione ";
      icon="icon2";
    }
    else if((url.indexOf("ledoff&")>0))
    {
     event="led";
     backtext="p-3 mb-2 bg-dark text-white";
     pre="Spegnimento ";
     icon="icon";
    }
    else if((url.indexOf("luce&")>0))
    {
      event="luce";
      backtext="p-3 mb-2 bg-light text-dark";
      pre="Accensione ";
      icon="icon2";
    }
    else if((url.indexOf("luceoff&")>0))
    {
     event="luce";
     backtext="p-3 mb-2 bg-dark text-white";
     pre="Spegnimento ";
     icon="icon";
    }
    else if((url.indexOf("usb&")>0))
    {
      event="usb";
      backtext="p-3 mb-2 bg-light text-dark";
      pre="Accensione ";
      icon="icon2";
    }
    else if((url.indexOf("usboff&")>0))
    {
     event="usb";
     backtext="p-3 mb-2 bg-dark text-white";
     pre="Spegnimento ";
     icon="icon";
    }
    else if((url.indexOf("presa&")>0))
    {
     event="presa";
     backtext="p-3 mb-2 bg-light text-dark";
     pre="Accensione ";
     icon="icon2";
    }
    else if((url.indexOf("presaoff&")>0))
    {
     event="presa";
     backtext="p-3 mb-2 bg-dark text-white";
     pre="Spegnimento ";
     icon="icon";
    }
    else if((url.indexOf("buongiorno&")>0))
    {
     event="buongiorno";
     backtext="p-3 mb-2 bg-light text-dark";
     pre="Accensione ";
     icon="icon2";
    }
    else if((url.indexOf("buonanotte&")>0))
    {
     event="buonanotte";
     backtext="p-3 mb-2 bg-dark text-white";
     pre="Spegnimento ";
     icon="icon";
    }
    else
     event="IN";
      
   if(time>0)
   {
   
  let timerInterval
Swal.fire({
  title: 'Spegnimento tra',
  html: '<b></b> millisecondi',
  timer: time+'000',
  timerProgressBar: true,
  allowOutsideClick: false,
  onBeforeOpen: () => {
    Swal.showLoading()
    timerInterval = setInterval(() => {
      Swal.getContent().querySelector('b')
        .textContent = Swal.getTimerLeft()
    }, 100)
  },
  onClose: () => {
    Swal.fire({
  position: 'top-end',
  type: 'success',
  title: 'Completato',
  html: "Spegnimento temporizzato completato",
  showConfirmButton: false,
  allowOutsideClick: true,
 
  //width:"10%"
  //timer: 2500
})
  }
}).then((result) => {
  if (
    /* Read more about handling dismissals below */
    result.dismiss === Swal.DismissReason.timer
  ) {
    console.log('I was closed by the timer') // eslint-disable-line
  }
})
   
   }
   else
   	mostracaricamento(pre+event+" in corso");

    

    //MANDO LA RICHIESTA AL SERVER
    const Http = new XMLHttpRequest();
    Http.open("GET", url);
    
    Http.timeout=60000; //Aspetto un minuto per la risposta del server


    //QUANDO HO COMPLETATO LE RICHIESTE
    Http.onreadystatechange = (e) => {
    
        if(Http.status == 0 && time==-1)
         {
         window.onbeforeunload = null;
         Swal.close()
         if(event=="buongiorno"||event=="buonanotte")
         {
         	document.getElementById("led").className =backtext;
            document.getElementById("luce").className =backtext;
            
            document.getElementById("led"+icon).style.display = "inline";
         if(icon.includes("2"))
         	document.getElementById("led"+icon.substring(0, icon.length-1)).style.display = "none";
         else
         	document.getElementById("led"+icon+"2").style.display = "none";
            document.getElementById("luce"+icon).style.display = "inline";
         if(icon.includes("2"))
         	document.getElementById("luce"+icon.substring(0, icon.length-1)).style.display = "none";
         else
         	document.getElementById("luce"+icon+"2").style.display = "none";
         }
         else
         {
         document.getElementById(event).className =backtext;
         document.getElementById(event+icon).style.display = "inline";
         if(icon.includes("2"))
         	document.getElementById(event+icon.substring(0, icon.length-1)).style.display = "none";
         else
         	document.getElementById(event+icon+"2").style.display = "none";
         }
           Swal.fire({
  position: 'top-end',
  type: 'success',
  title: 'Completato',
  html: pre+event+" completato",
  showConfirmButton: false,
  allowOutsideClick: true,
 
  //width:"10%"
  //timer: 2500
})
         }
        }

    //SE IL SERVER VA IN TIMEOUT
    Http.ontimeout=function(e)
    {
       
  }

    Http.abort=function(e)
    {
      console.log("Unexpeted error!");
    }

    Http.onerror=function(e)
    {
    //$(".ui-dialog:visible").find(".dialog").dialog("close");
    
    }

    Http.send();

}

function buonanotte()
{
 
 Swal.fire({
  title: 'Inserisci i secondi prima dello spegnimento delle luci',
  input: 'number',
  inputAttributes: {
    autocapitalize: 'off'
  },
  showCancelButton: true,
  confirmButtonText: 'OK',
  cancelButtonText: 'Annulla',
  showLoaderOnConfirm: true
  }).then((result) => {
  if (result.value) {
    makerequest('REQUEST'+result.value/60+'&KEY',result.value);
  }
})
 
 
}